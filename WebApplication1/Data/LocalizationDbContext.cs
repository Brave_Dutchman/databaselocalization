﻿using Localization.Framework;
using Microsoft.EntityFrameworkCore;

namespace WebApplication1.Data
{
    public class LocalizationDbContext : DbContext, ILocalizationDbContext
    {
        public LocalizationDbContext(DbContextOptions<LocalizationDbContext> options) : base(options)
        { }

        public DbSet<LocalizationEntry> Localizations { get; set; }
    }
}
