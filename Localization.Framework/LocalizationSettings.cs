﻿using System.Collections.Generic;

namespace Localization.Framework
{
    public class LocalizationSettings
    {
        public string FallBackCulture { get; set; }

        public HashSet<char> PathSeperators { get; set; }
    }
}
