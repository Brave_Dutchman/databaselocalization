﻿namespace Localization.Framework
{
    public class LocalizationEntry
    {
        public int Id { get; set; }

        public string Path { get; set; }

        public string Language { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }

        public string Info { get; set; }
    }
}
