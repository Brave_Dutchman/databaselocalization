﻿using System.Collections.Generic;
using System.Globalization;
using Microsoft.Extensions.Localization;

namespace Localization.Framework
{
    public interface ILocalizer
    {
        string this[string key] { get; }

        string this[string key, params object[] arguments] { get; }

        string GetString(string key);

        string GetString(string key, params object[] arguments);

        ILocalizer WithCulture(CultureInfo culture);

        IEnumerable<LocalizedString> GetAllStrings();
    }
}