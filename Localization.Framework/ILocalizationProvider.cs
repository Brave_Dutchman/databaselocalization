﻿using System.Linq;

namespace Localization.Framework
{
    public interface ILocalizationProvider
    {
        IQueryable<LocalizationEntry> Localizations();
    }
}
