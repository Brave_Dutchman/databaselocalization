﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Microsoft.Extensions.Localization;

namespace Localization.Framework.Internal
{
    public class LocalizationCache
    {
        private readonly Dictionary<string, LanguageLocalizationEnties> _pathLanguageMapping;

        public LocalizationCache()
        {
            _pathLanguageMapping = new Dictionary<string, LanguageLocalizationEnties>();
        }

        public LanguageLocalizationEnties this[string path]
        {
            get
            {
                return _pathLanguageMapping[path];
            }
        }

        public LanguageLocalizationEnties GetCulture(string path)
        {
            return _pathLanguageMapping[path];
        }

        public bool ContainsKey(string path)
        {
            return _pathLanguageMapping.ContainsKey(path);
        }

        public void AddRange(IDictionary<string, LanguageLocalizationEnties> pairs)
        {
            foreach (var pair in pairs)
            {
                if (!_pathLanguageMapping.ContainsKey(pair.Key))
                {
                    _pathLanguageMapping.Add(pair.Key, pair.Value);
                }
            }
        }

        public IEnumerable<LocalizedString> GetAllStrings(string path)
        {
            if (_pathLanguageMapping.ContainsKey(path))
            {
                var languageMappings = _pathLanguageMapping[path];
                return languageMappings.GetAllStrings(CultureInfo.CurrentUICulture.Name);
            }

            return Array.Empty<LocalizedString>();
        }
    }
}
