﻿using System.Collections.Generic;
using System.Globalization;
using Microsoft.Extensions.Localization;

namespace Localization.Framework.Internal
{
    public class Localizer<T> : ILocalizer<T>
    {
        private readonly ILocalizer _localizer;

        public Localizer(LocalizerFactory localizerFactory)
        {
            _localizer = localizerFactory.Create(typeof(T).FullName);
        }

        public string this[string key]
        {
            get
            {
                return GetString(key);
            }
        }

        public string this[string key, params object[] arguments]
        {
            get
            {
                return GetString(key, arguments);
            }
        }

        public IEnumerable<LocalizedString> GetAllStrings()
        {
            return _localizer.GetAllStrings();
        }

        public string GetString(string key)
        {
            return _localizer.GetString(key);
        }

        public string GetString(string key, params object[] arguments)
        {
            return _localizer.GetString(key, arguments);
        }

        public ILocalizer<T> WithCulture(CultureInfo culture)
        {
            _localizer.WithCulture(culture);
            return this;
        }

        ILocalizer ILocalizer.WithCulture(CultureInfo culture)
        {
            _localizer.WithCulture(culture);
            return this;
        }
    }
}
