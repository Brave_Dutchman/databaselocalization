﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Localization.Framework.Internal
{
    public class LocalizationLoader
    {
        private readonly LocalizationCache _localizationCache;
        private readonly IServiceProvider _serviceProvider;
        private readonly LocalizationSettings _settings;

        public LocalizationLoader(IOptions<LocalizationSettings> options, LocalizationCache localizationCache, IServiceProvider serviceProvider)
        {
            _localizationCache = localizationCache;
            _serviceProvider = serviceProvider;
            _settings = options.Value;
        }

        public bool IsLoaded(string path)
        {
            return _localizationCache.ContainsKey(path);
        }

        public void Load(string path)
        {
            string[] Parts = _createPathParts(path);

            using (var scope = _serviceProvider.CreateScope())
            {
                var provider = scope.ServiceProvider.GetRequiredService<ILocalizationProvider>();
                
                var entries = provider.Localizations()
                    .Where(e => Parts.Contains(e.Path))
                    .OrderByDescending(x => x.Path)
                    .ToArray();

                var dict = Parts.ToDictionary(p => p, p => new LanguageLocalizationEnties(entries.Where(x => x.Path.Length <= p.Length)));
                _localizationCache.AddRange(dict);
            }
        }
        
        private string[] _createPathParts(string path)
        {
            List<string> parts = new List<string>();
            for (int i = 0; i <= path.Length - 1; i++)
            {
                if (_settings.PathSeperators.Contains(path[i]))
                {
                    var substring = path.Substring(0, i);
                    if (substring == string.Empty)
                    {
                        continue;
                    }

                    parts.Add(substring);
                }
            }

            parts.Add(path);
            return parts.ToArray();
        }
    }
}
