﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Localization;

namespace Localization.Framework.Internal
{
    public class KeyLocalizationEnties
    {
        private readonly Dictionary<string, string> _keyValueMappings;

        public KeyLocalizationEnties(IEnumerable<LocalizationEntry> entries)
        {
            _keyValueMappings = new Dictionary<string, string>();

            foreach (var entry in entries)
            {
                if (!_keyValueMappings.ContainsKey(entry.Key))
                {
                    _keyValueMappings.Add(entry.Key, entry.Value);
                }
            }
        }

        public string this[string key]
        {
            get
            {
                return _keyValueMappings[key];
            }
        }

        public string GetValue(string key)
        {
            return _keyValueMappings[key];
        }

        public bool ContainsKey(string key)
        {
            return _keyValueMappings.ContainsKey(key);
        }

        public IEnumerable<LocalizedString> GetAllStrings()
        {
            return _keyValueMappings
                .Select(x => new LocalizedString(x.Key, x.Value));
        }
    }
}
