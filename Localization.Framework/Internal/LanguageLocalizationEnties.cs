﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Localization;

namespace Localization.Framework.Internal
{
    public class LanguageLocalizationEnties
    {
        private readonly Dictionary<string, KeyLocalizationEnties> _languageEntriesMappings;

        public LanguageLocalizationEnties(IEnumerable<LocalizationEntry> entries)
        {
            _languageEntriesMappings = entries.GroupBy(x => x.Language).ToDictionary(x => x.Key, x => new KeyLocalizationEnties(x));
        }

        public KeyLocalizationEnties this[string language]
        {
            get
            {
                return _languageEntriesMappings[language];
            }
        }

        public KeyLocalizationEnties GetValue(string language)
        {
            return _languageEntriesMappings[language];
        }

        public bool ContainsKey(string language)
        {
            return _languageEntriesMappings.ContainsKey(language);
        }

        public IEnumerable<LocalizedString> GetAllStrings(string language)
        {
            if (_languageEntriesMappings.ContainsKey(language))
            {
                return _languageEntriesMappings[language].GetAllStrings();
            }

            return Array.Empty<LocalizedString>();
        }
    }
}
