﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Localization;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Localization;

namespace Localization.Framework.Internal
{
    public class ViewLocalizer : IViewLocalizer, IViewContextAware
    {
        private readonly LocalizerFactory _localizerFactory;
        private readonly IHostingEnvironment _hostingEnvironment;

        private ILocalizer _localizer;

        public ViewLocalizer(LocalizerFactory localizerFactory, IHostingEnvironment hostingEnvironment)
        {
            _localizerFactory = localizerFactory;
            _hostingEnvironment = hostingEnvironment;
        }

        public LocalizedHtmlString this[string name]
        {
            get
            {
                var value = _localizer.GetString(name);
                return new LocalizedHtmlString(name, value);
            }
        }

        public LocalizedHtmlString this[string name, params object[] arguments]
        {
            get
            {
                var value = _localizer.GetString(name, arguments);
                return new LocalizedHtmlString(name, value);
            }
        }

        public void Contextualize(ViewContext viewContext)
        {
            var path = viewContext.ExecutingFilePath;

            if (string.IsNullOrEmpty(path))
            {
                path = viewContext.View.Path;
            }

            var fullPath = _buildBaseName(path, _hostingEnvironment.ApplicationName);
            _localizer = _localizerFactory.Create(fullPath);
        }

        public IEnumerable<LocalizedString> GetAllStrings(bool includeParentCultures)
        {
            return _localizer.GetAllStrings();
        }

        public LocalizedString GetString(string name)
        {
            var value = _localizer.GetString(name);
            return new LocalizedString(name, value);
        }

        public LocalizedString GetString(string name, params object[] arguments)
        {
            var value = _localizer.GetString(name, arguments);
            return new LocalizedString(name, value);
        }

        public IHtmlLocalizer WithCulture(CultureInfo culture)
        {
            _localizer.WithCulture(culture);
            return this;
        }

        private string _buildBaseName(string path, string applicationName)
        {
            var extension = Path.GetExtension(path);
            var startIndex = path[0] == '/' || path[0] == '\\' ? 1 : 0;
            var length = path.Length - startIndex - extension.Length;
            var capacity = length + applicationName.Length + 1;
            var builder = new StringBuilder(path, startIndex, length, capacity);

            builder.Replace('/', '.').Replace('\\', '.');

            // Prepend the application name
            builder.Insert(0, '.');
            builder.Insert(0, applicationName);

            return builder.ToString();
        }
    }
}
