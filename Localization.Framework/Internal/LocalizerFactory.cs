﻿using System.Globalization;
using Microsoft.Extensions.Options;

namespace Localization.Framework.Internal
{
    public class LocalizerFactory
    {
        private readonly LocalizationCache _localizationCache;
        private readonly LocalizationLoader _localizationLoader;
        private readonly IOptions<LocalizationSettings> _options;

        public LocalizerFactory(LocalizationCache localizationCache, LocalizationLoader localizationLoader, IOptions<LocalizationSettings> options)
        {
            _localizationCache = localizationCache;
            _localizationLoader = localizationLoader;
            _options = options;
        }

        public ILocalizer Create(string path, CultureInfo culture)
        {
            return Create(path).WithCulture(culture);
        }

        public ILocalizer Create(string path)
        {
            if (!_localizationLoader.IsLoaded(path))
            {
                _localizationLoader.Load(path);
            }

            return new Localizer(path, _localizationCache, _options);
        }
    }
}
