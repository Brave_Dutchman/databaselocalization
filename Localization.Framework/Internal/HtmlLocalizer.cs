﻿using System.Collections.Generic;
using System.Globalization;
using Microsoft.AspNetCore.Mvc.Localization;
using Microsoft.Extensions.Localization;

namespace Localization.Framework.Internal
{
    public class HtmlLocalizer<T> : IHtmlLocalizer<T>
    {
        private readonly ILocalizer _localizer;

        public HtmlLocalizer(LocalizerFactory localizerFactory)
        {
            _localizer = localizerFactory.Create(typeof(T).FullName);
        }

        public LocalizedHtmlString this[string name]
        {
            get
            {
                var value = _localizer.GetString(name);
                return new LocalizedHtmlString(name, value);
            }
        }

        public LocalizedHtmlString this[string name, params object[] arguments]
        {
            get
            {
                var value = _localizer.GetString(name, arguments);
                return new LocalizedHtmlString(name, value);
            }
        }

        public IEnumerable<LocalizedString> GetAllStrings(bool includeParentCultures)
        {
            return _localizer.GetAllStrings();
        }

        public LocalizedString GetString(string name)
        {
            var value = _localizer.GetString(name);
            return new LocalizedString(name, value);
        }

        public LocalizedString GetString(string name, params object[] arguments)
        {
            var value = _localizer.GetString(name, arguments);
            return new LocalizedString(name, value);
        }

        public IHtmlLocalizer WithCulture(CultureInfo culture)
        {
            _localizer.WithCulture(culture);
            return this;
        }
    }
}
