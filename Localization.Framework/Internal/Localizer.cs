﻿using System.Collections.Generic;
using System.Globalization;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;

namespace Localization.Framework.Internal
{
    public class Localizer : ILocalizer
    {
        private readonly string _path;
        private readonly LocalizationCache _localizationCache;
        private readonly LocalizationSettings _settings;

        private CultureInfo _culture;

        public Localizer(string path, LocalizationCache localizationCache, IOptions<LocalizationSettings> options)
        {
            _path = path;
            _localizationCache = localizationCache;
            _settings = options.Value;
        }

        protected CultureInfo GetCulture()
        {
            return _culture ?? CultureInfo.CurrentUICulture;
        }

        public string this[string key]
        {
            get
            {
                return GetString(key);
            }
        }

        public string this[string key, params object[] arguments]
        {
            get
            {
                return GetString(key, arguments);
            }
        }

        public string GetString(string key)
        {
            var culture = GetCulture();
            var languages = _localizationCache[_path];

            if (languages.ContainsKey(culture.Name))
            {
                var language = languages[culture.Name];
                if (language.ContainsKey(key))
                {
                    return language[key];
                }
            }

            var parentCulture = culture.Parent;
            if (parentCulture != CultureInfo.InvariantCulture && languages.ContainsKey(parentCulture.Name))
            {
                var language = languages[parentCulture.Name];
                if (language.ContainsKey(key))
                {
                    return language[key];
                }
            }

            if (!string.IsNullOrEmpty(_settings.FallBackCulture) && languages.ContainsKey(_settings.FallBackCulture))
            {
                var language = languages[_settings.FallBackCulture];
                if (language.ContainsKey(key))
                {
                    return language[key];
                }
            }

            return key;
        }

        public string GetString(string key, params object[] arguments)
        {
            return string.Format(GetString(key), arguments);
        }

        public ILocalizer WithCulture(CultureInfo culture)
        {
            _culture = culture;
            return this;
        }

        public IEnumerable<LocalizedString> GetAllStrings()
        {
            return _localizationCache.GetAllStrings(_path);
        }
    }
}
