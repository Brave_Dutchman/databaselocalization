﻿using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Localization.Framework.Internal
{
    public class LocalizationProvider<TContext> : ILocalizationProvider
        where TContext : DbContext, ILocalizationDbContext
    {
        private readonly TContext _dbContext;

        public LocalizationProvider(TContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<LocalizationEntry> Localizations() => _dbContext.Localizations;
    }
}
