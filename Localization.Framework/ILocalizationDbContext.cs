﻿using Microsoft.EntityFrameworkCore;

namespace Localization.Framework
{
    public interface ILocalizationDbContext
    {
        DbSet<LocalizationEntry> Localizations { get; set; }
    }
}