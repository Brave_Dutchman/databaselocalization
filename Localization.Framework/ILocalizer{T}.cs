﻿using System.Globalization;

namespace Localization.Framework
{
    public interface ILocalizer<T> : ILocalizer
    {
        new ILocalizer<T> WithCulture(CultureInfo culture);
    }
}
