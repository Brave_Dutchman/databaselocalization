﻿using Localization.Framework;
using Localization.Framework.Internal;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class RegistrationExtentions
    {
        public static IServiceCollection AddDatabaseLocalization<TContext>(this IServiceCollection services, IConfigurationSection localizationSection)
            where TContext : DbContext, ILocalizationDbContext
        {
            services.Configure<LocalizationSettings>(localizationSection);

            services.AddSingleton<LocalizationCache>();
            services.AddSingleton<LocalizationLoader>();
            services.AddSingleton<LocalizerFactory>();

            services.AddScoped<ILocalizationProvider, LocalizationProvider<TContext>>();
            
            services.AddTransient(typeof(ILocalizer<>), typeof(Localizer<>));
            services.Replace(ServiceDescriptor.Transient<AspNetCore.Mvc.Localization.IViewLocalizer, ViewLocalizer>());
            services.Replace(ServiceDescriptor.Transient(typeof(AspNetCore.Mvc.Localization.IHtmlLocalizer<>), typeof(HtmlLocalizer<>)));

            return services;
        }
    }
}
